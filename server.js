const express = require("express");
const mongoose = require('mongoose');
require('dotenv').config();
const bodyParser = require('body-parser');
const logger = require('./logger');

const app = express();
app.use(bodyParser.json());
const habitrouter = require('./app/routers/habit');

app.use('/habit', habitrouter);

const mongoUrl = process.env.MONGO_URL;

mongoose
  .connect(mongoUrl)
  .then(() => {
    logger.info("Successfully connect to MongoDB.");
    logger.info(mongoUrl);
    startServer();
  })
  .catch(err => {
    logger.error("Connection error", err);
    process.exit();
  });

function startServer() {
  const PORT = process.env.API_PORT || 3000;
  app.listen(PORT, () => {
    logger.info(`Server is running on port ${PORT}.`);
  });
}




module.exports = app;
