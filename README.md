# Habittracker-backend


## How to run

```sh
git clone hhttps://gitlab.com/swat5020110/habittracker-backend.git
```

### Server RUN
```sh
npm install
node .\server.js
```

### Test
```sh
npm install
npm run test
```

### Test
```sh
npm install
npm run lint
```

### Pipeline
Die Pipelines sind unter diesem Link einsehbar.
https://gitlab.com/swat5020110/habittracker-backend/-/pipelines
