const mongoose = require("mongoose");
const app = require("../server")
const request = require('supertest');


const mongoUrl = process.env.MONGO_URL;

beforeEach(async () => {
    await mongoose.connect(mongoUrl)
})

afterEach(async () => {
    await mongoose.connection.close();
});

describe('HabitController', () => {
    describe('GET /habit', () => {
        it('should return all habits', async () => {
            const res = await request(app).get("/habit/getAll");
            expect(res.statusCode).toBe(200);
            expect(res.body.length).toBeGreaterThan(0);
        });
    });

    describe('GET /habit/getHabitById/1', () => {
        it('should return a habit by ID', async () => {
            const res = await request(app).get("/habit/getHabitById/1");
            expect(res.statusCode).toBe(200);
        });
    });

    describe('GET /habit/getHabitById/1000', () => {
        it('should return a habit by ID', async () => {
            const res = await request(app).get("/habit/getHabitById/1000");
            expect(res.statusCode).toBe(404);
            expect(res.body.message).toEqual('Habit not found');            
        });
    });


    describe('GET /habit/getHabitByUserId/1', () => {
        it('should return habits by user ID', async () => {
            const res = await request(app).get("/habit/getHabitsByUserId/1");
            expect(res.statusCode).toBe(200);
            expect(res.body.length).toBeGreaterThan(0);
        });
    });


    describe('GET /habit/getHabitByUserId/9999', () => {
        it('should return habits by user ID', async () => {
            const res = await request(app).get("/habit/getHabitsByUserId/9999");
            expect(res.statusCode).toBe(404);
            expect(res.body.message).toEqual('No habits found for this user'); 
        });
    });
});
