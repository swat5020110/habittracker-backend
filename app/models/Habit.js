const mongoose = require('mongoose');

const habitSchema = new mongoose.Schema({
  id: {
    type: Number,
    required: false
  },
  user_Id: {
    type: Number,
    required: false
  },
  name: {
    type: String,
    required: false
  },
  description: {
    type: String,
    required: false
  },
  time: {
    type: String,
    required: false
  },
  place: {
    type: String,
    required: false
  },
  next_Habit_Id: {
    type: Number,
    required: false
  },
  days: {
    type: Number,
    required: false
  }
});

module.exports = mongoose.model('Habit', habitSchema);
