const User = require('../models/user');

exports.register = (req, res) => {
  const { username, email, password } = req.body;
  const user = new User({ username, email, password });
  user.save((err, user) => {
    if (err) {
      console.log(err);
      return res.status(400).json({ error: 'Could not register user' });
    }
    res.json(user);
  });
};
