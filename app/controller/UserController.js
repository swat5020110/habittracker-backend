const User = require('../models/user');

exports.register = function(req, res) {
  const { name, email, password } = req.body;

  // Check if the user already exists
  User.findOne({ email }, function(err, existingUser) {
    if (err) {
      return res.status(500).send({ error: 'Something went wrong.' });
    }

    if (existingUser) {
      return res.status(409).send({ error: 'User already exists.' });
    }

    // Create a new user
    const newUser = new User({ name, email, password });
    newUser.save(function(err) {
      if (err) {
        return res.status(500).send({ error: 'Something went wrong.' });
      }

      return res.status(200).send({ message: 'User registered successfully.' });
    });
  });
};
