const Habit = require('../models/Habit');
const logger = require('../../logger');

async function getAllHabits(req, res) {
    try {
        const habits = await Habit.find();
        res.status(200).json(habits);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

async function getHabitById(req, res) {
    try {
        const habit = await Habit.findOne({id :req.params.habitId});
        if (!habit) {
            logger.error(`Habit with id ${req.params.habitId} not found`);
            return res.status(404).json({ message: 'Habit not found' });
        }
        res.status(200).json(habit);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

async function getHabitByUserId(req, res) {
    try {
        const habits = await Habit.find({ user_Id: req.params.userId });
        if (!habits.length) {
            logger.error(`No habits found for user with id ${req.params.userId}`);
            return res.status(404).json({ message: 'No habits found for this user' });
        }
        res.status(200).json(habits);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

async function createHabit(req, res) {
    try {
        const {id, user_id, name, description, time, place, next_Habit_Id, days } = req.body;
        const newHabit = new Habit({id, user_id, name, description, time, place, next_Habit_Id, days  })
        await newHabit.save();
        res.status(201).json(newHabit);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
}

async function updateHabit(req, res) {
    try {
        const updatedHabit = await Habit.findByIdAndUpdate(req.params.habitId, req.body, { new: true });
        if (!updatedHabit) {
            return res.status(404).json({ message: 'Habit not found' });
        }
        res.status(200).json(updatedHabit);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
}

async function deleteHabit(req, res) {
    try {
        const deletedHabit = await Habit.findByIdAndDelete(req.params.habitId);
        if (!deletedHabit) {
            return res.status(404).json({ message: 'Habit not found' });
        }
        res.status(200).json({ message: 'Habit deleted successfully' });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
}

module.exports = {
    getAllHabits,
    getHabitById,
    getHabitByUserId,
    createHabit,
    updateHabit,
    deleteHabit
};
