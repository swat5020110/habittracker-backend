const express = require('express');
const mongoose = require('mongoose');

const app = express();
app.use(express.json());

// Connect to MongoDB
mongoose.connect('mongodb+srv://vithu:J2fjjgsW7Jy2k4JK@habittracker.sk0ahjf.mongodb.net/?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true 
})
    .then(() => console.log('Connected to database'))
    .catch((err) => console.error(err));

// Define the user schema
const UserSchema = new mongoose.Schema({
    username: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true }
});

const User = mongoose.model('User', UserSchema);

// Define the API routes
app.post('/api/user', (req, res) => {
    const { username, email, password } = req.body;

    // Check if the user already exists
    User.findOne({ email })
        .then((user) => {
            if (user) {
                res.status(409).send('User already exists');
            } else {
                // Create a new user
                const newUser = new User({ username, email, password });
                newUser.save()
                    .then(() => res.status(201).send('User created'))
                    .catch((err) => {
                        console.error(err);
                        res.status(500).send('Internal server error');
                    });
            }
        })
        .catch((err) => {
            console.error(err);
            res.status(500).send('Internal server error');
        });
});

// Start the server
app.listen(3000, () => {
    console.log('Server listening on port 3000');
    });
    
    
   