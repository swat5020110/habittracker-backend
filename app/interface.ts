export interface IUser{
    id: number,
    username: string,
    email: string,
    password: string
}


export interface IHabit{
    id: number,
    user_id:number, 
    name: string, 
    description: string, 
    time: string, 
    place: string,  
    next_Habit_id: number 
    days: number
}