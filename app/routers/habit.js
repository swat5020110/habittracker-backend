const express = require('express');
const HabitController = require('../controller/HabitController');

const router = express.Router();

router.get('/getAll', HabitController.getAllHabits);
router.get('/getHabitById/:habitId', HabitController.getHabitById);
router.get('/getHabitsByUserId/:userId', HabitController.getHabitByUserId);
router.post('/', HabitController.createHabit);
router.put('/:id', HabitController.updateHabit);
router.delete('/:id', HabitController.deleteHabit);

module.exports = router;